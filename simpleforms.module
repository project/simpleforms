<?php

/**
 * @file
 * Simpleforms module.
 */

/*************/
/*** Hooks ***/
/*************/

/**
* Implementation of hook_menu().
*/
function simpleforms_menu() {
  $items['node/%node/simpleforms-results'] = array(
    'title' => 'Submission results',
    'page callback' => 'simpleforms_results_submissions',
    'page arguments' => array(1),
    'access callback' => 'node_access',
    'access arguments' => array('update', 1),
    'file' => 'simpleforms.pages.inc',
    'weight' => 2,
    'type' => MENU_LOCAL_TASK,
  );

  $items['node/%node/simpleforms-results/export'] = array(
    'title' => 'Submission results export',
    'page callback' => 'simpleforms_results_submissions_export',
    'page arguments' => array(1),
    'access callback' => 'node_access',
    'access arguments' => array('update', 1),
    'file' => 'simpleforms.pages.inc',
    'type' => MENU_CALLBACK,
  );
    
  $items['node/%node/simpleforms-result/%'] = array(
    'title' => 'View submission result',
    'page callback' => 'simpleforms_submission_page',
    'page arguments' => array(1, 3),
    'access callback' => 'node_access',
    'access arguments' => array('update', 1),
    'file' => 'simpleforms.pages.inc',
    'type' => MENU_CALLBACK,
  );

  $items['node/%node/simpleforms-result/%/delete'] = array(
    'title' => 'Delete submission result',
    'page callback' => 'simpleforms_submission_confirm_delete_page',
    'page arguments' => array(1, 3),
    'access callback' => 'node_access',
    'access arguments' => array('delete', 1),
    'file' => 'simpleforms.pages.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_node_info().
 */
function simpleforms_node_info() {
  return array(
    'simpleform' => array(
      'name' => t('Simple form'),
      'base' => 'simpleforms',
      'description' => t('A <em>simpleform</em> is a survey content type. It allows site users to create custom forms (like surveys) for which other site users can submit data.'),
      'title_label' => t('Title'),
      'locked' => TRUE,
    )
  );
}

/**
 * Implements hook_form().
 */
function simpleforms_form($node, $form_state) {
  $form = node_content_form($node, $form_state);

  drupal_add_css(drupal_get_path('module', 'simpleforms') . '/simpleforms.css');

  drupal_add_library('system', 'ui.sortable');

  $js_path = drupal_get_path('module', 'simpleforms') . '/js';

  drupal_add_js($js_path . '/json2.js');
  if (file_exists($js_path . '/jquery.scrollTo-min.js')) {
    drupal_add_js($js_path . '/jquery.scrollTo-min.js');
  }
  
  drupal_add_js($js_path . '/simpleforms.js');
  drupal_add_js($js_path . '/jquery.simpleforms.js');

  $form['simpleforms'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form'),
    '#weight' => 1,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  // This field is storing simpleform definition. This field is also updated
  // by simpleform js when user modify simpleform in it browser. On the end
  // we save this field on simpleform submit.
  $form['simpleforms']['simpleforms_form_definition'] = array(
    '#type' => 'hidden',
    '#default_value' => isset($node->simpleforms_form_definition)? filter_xss($node->simpleforms_form_definition) : '',
  );

  $form['simpleforms']['simpleform'] = array(
    '#markup' => '<div id="form-wrap"><ul id="form-builder"><li>Please enable javascript in order to edit simpleform.</li></ul></div>',
  );

  $form['simpleforms_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form settings'),
    '#group' => 'additional_settings',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['simpleforms_settings']['simpleform_submit_action'] = array(
    '#type' => 'select',
    '#title' => t('Action to take on form submission'),
    '#options' => array(
      'no_action' => t('No action'),
      'custom_message' => t('Show custom message'),
      'redirect' => t('Redirect'),
    ),
    '#default_value' => isset($node->simpleform_submit_action)? check_plain($node->simpleform_submit_action) : '',
  );
  $form['simpleforms_settings']['simpleform_submit_action_custom_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom message text'),
    '#states' => array(
      'visible' => array(':input[name="simpleform_submit_action"]' => array('value' => 'custom_message')),
    ),
    '#default_value' => isset($node->simpleform_submit_action_custom_message)? check_plain($node->simpleform_submit_action_custom_message) : '',
  );

  $default_value_redirect = '';
  if (isset($node->simpleform_submit_action_redirect)) {
    $default_value_redirect = ($node->simpleform_submit_action_redirect != '<front>')? check_url($node->simpleform_submit_action_redirect) : '<front>';
  }
  $form['simpleforms_settings']['simpleform_submit_action_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect URL'),
    '#description' => t('Input absolute URL or page path, for example http://example.com or node/123.'),
    '#states' => array(
      'visible' => array(':input[name="simpleform_submit_action"]' => array('value' => 'redirect')),
    ),
    '#default_value' => $default_value_redirect,
  );

  $form['simpleforms_settings']['simpleform_submit_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send an email on form submission'),
    '#default_value' => (isset($node->simpleform_submit_email) && $node->simpleform_submit_email)? TRUE : FALSE,
  );
  $form['simpleforms_settings']['simpleform_submit_email_values'] = array(
    '#type' => 'textarea',
    '#title' => t('Email addresses'),
    '#description' => t('You can enter multiple emails, add one email address per line.'),
    '#states' => array(
      'visible' => array(':input[name="simpleform_submit_email"]' => array('checked' => TRUE)),
    ),
    '#default_value' => isset($node->simpleform_submit_email_values)? check_plain($node->simpleform_submit_email_values) : '',
  );

  return $form;
}

/**
* Implements hook_validate().
*/
function simpleforms_validate($node, $form, &$form_state) {
  if ($node->simpleform_submit_action == 'custom_message' && empty($node->simpleform_submit_action_custom_message)) {
    form_set_error('simpleform_submit_action_custom_message', t('Please enter custom message text.'));
  }

  if ($node->simpleform_submit_action == 'redirect') {
    if (empty($node->simpleform_submit_action_redirect)) {
      form_set_error('simpleform_submit_action_redirect', t('Please enter redirect URL.'));
    }
    elseif (!drupal_valid_path($node->simpleform_submit_action_redirect)) {
      form_set_error('simpleform_submit_action_redirect', t('Please enter a valid URL.'));
    }
  }

  if ($node->simpleform_submit_email) {
    if (empty($node->simpleform_submit_email_values)) {
      form_set_error('simpleform_submit_email_values', t('Please enter email addresses.'));
    }
    else {
      
      $emails = preg_split("/\n/", $node->simpleform_submit_email_values);
      foreach ($emails as $email) {
        $email = trim($email);
        if (!valid_email_address($email)) {
          form_set_error('simpleform_submit_email_values', t('Entered email @email is not valid.', array('@email' => $email)));
        }
      }
    }
  }
}

/**
 * Implements hook_insert().
 */
function simpleforms_insert($node) {
  simpleforms_node_save($node);
}

/**
 * Implements hook_update().
 */
function simpleforms_update($node) {
  simpleforms_node_save($node);
}

/**
 * Implements hook_delete().
 */
function simpleforms_delete($node) {
  simpleforms_db_delete_form($node->nid);
  simpleforms_db_delete_all_submissions($node->nid);
}

/**
 * Implements hook_load().
 */
function simpleforms_load($nodes) {
  foreach ($nodes as $node) {
    // Load simpleform.
    if ($form = _simpleforms_db_load_form($node->nid)) {
      // Form definition.
      $node->simpleforms_form_definition = $form['form_definition'];

      $form_options = unserialize($form['form_options']);

      // Form options.
      $node->simpleform_submit_action = $form_options['simpleform_submit_action'];
      $node->simpleform_submit_action_custom_message = $form_options['simpleform_submit_action_custom_message'];
      $node->simpleform_submit_action_redirect = $form_options['simpleform_submit_action_redirect'];
      $node->simpleform_submit_email = $form_options['simpleform_submit_email'];
      $node->simpleform_submit_email_values = $form_options['simpleform_submit_email_values'];
    }
  }

  return $node;
}

/**
 * Implement hook_view().
 */
function simpleforms_view($node, $view_mode) {
  // Show simpleform form.
  if ($view_mode == 'full') {
    $form_definition = drupal_json_decode($node->simpleforms_form_definition);
    $node->content['simpleform'] = array(
      'form' => drupal_get_form('simpleforms_submit_form', $node->nid, $form_definition),
      '#weight' => 10,
    );
  }
  return $node;
}

/**
 * Implements hook_mail().
 */
function simpleforms_mail($key, &$message, $params) {
  switch ($key) {
    case 'submission':
      $langcode = $message['language']->language;
      $site = variable_get('site_name', '');

      $message['subject'] = t('Notification from !site - simpleform submission', array('!site' => $site), array('langcode' => $langcode));
      $message['body'][] = t("Simpleform submission for !node_url.\nYou can see submited data on !data_url",
        array(
          '!node_url' => url('node/' . $params['nid'], array('absolute' => TRUE)),
          '!data_url' => url('node/' . $params['nid'] . '/simpleforms-result/' . $params['sid'], array('absolute' => TRUE)),
        ),
        array('langcode' => $langcode)
      );
      break;
  }
}

/**
 * Implementation of hook_theme().
 */
function simpleforms_theme() {
  return array(
    'simpleforms_submission_author' => array(
      'variables' => array('submission', 'account'),
      'file' => 'simpleforms.theme.inc',
    ),
    'simpleforms_scale_radios' => array(
      'render element' => 'element',
      'file' => 'simpleforms.theme.inc',
    ),
    'simpleforms_scale_radio' => array(
      'render element' => 'element',
      'file' => 'simpleforms.theme.inc',
    ),
  );
}

/************************/
/*** Helper functions ***/
/************************/

/**
 * Helper function for saving simpleform definition on node insert or update.
 *
 * @param $node
 * @param $form_definition
 */
function simpleforms_node_save($node) {
  simpleforms_db_delete_form($node->nid);

  // Prepare form options.
  $form_options['simpleform_submit_action'] = $node->simpleform_submit_action;
  $form_options['simpleform_submit_action_custom_message'] = $node->simpleform_submit_action_custom_message;
  $form_options['simpleform_submit_action_redirect'] = $node->simpleform_submit_action_redirect;
  $form_options['simpleform_submit_email'] = $node->simpleform_submit_email;
  $form_options['simpleform_submit_email_values'] = $node->simpleform_submit_email_values;
  $form_options = serialize($form_options);

  simpleforms_db_save_form($node->nid, $node->simpleforms_form_definition, $form_options);
}

/**
 * Builds submit form using Drupal FAPI.
 *
 * @param $entity_id
 *   Entity id of entity we are building form. For example node id.
 * @param $form_def
 *   Simpleform form definition.
 * @param $sid
 *   Submission id. If defined, it will load submission result and set default
 *   values for form fields; forms fields will be readonly in this case.
 * @return
 *   Builded Drupal form array.
 */
function simpleforms_submit_form($form, &$form_state, $entity_id, $form_def, $sid = NULL) {
  return _simpleforms_build_drupal_form($form, $form_state, $entity_id, $form_def, $sid);
}

function simpleforms_submit_form_validate($form, &$form_state) {
  // Validation logic.
  // @TODO - For custom scale element we should do validation if it is required.
}

function simpleforms_submit_form_submit($form, &$form_state) {
  $nid = $form_state['values']['entity_id'];

  // Prepare submitted data for saving.
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, 'simpleforms_') === 0) {
      $data[$key] = $value;
    }
  }
  $data = serialize($data);
  if (empty($data)) {
    return;
  }

  // Save submitted data.
  $sid = simpleforms_db_save_submission($nid, $data);
  watchdog('simpleforms', 'Submitted simpleform for node !nid. Submission sid is !sid', array('!nid' => $nid, '!sid' => $sid));

  // Now let's execute any custom submit action.

  // Load simpleform data.
  $simpleform = simpleforms_db_load_form($form_state['values']['entity_id']);
  $options = & $simpleform['form_options'];

  // Mail sending.
  if ($options['simpleform_submit_email']) {
    // Get email address for sending.
    $to = array();
    $emails = preg_split("/\n/", $options['simpleform_submit_email_values']);
    foreach ($emails as $email) {
      $to[] = trim($email);
    }
    $to = join(', ', $to);

    $params = array('nid' => $nid, 'sid' => $sid);

    drupal_mail('simpleforms', 'submission', $to, language_default(), $params);
  }

  // Other custom actions.
  switch ($options['simpleform_submit_action']) {
    case 'no_action':
      drupal_set_message(t('Thank you for your submission.'));
      break;
    case 'custom_message':
      drupal_set_message(check_plain($options['simpleform_submit_action_custom_message']));
      break;
    case 'redirect':
      drupal_goto($options['simpleform_submit_action_redirect']);
      break;
  }
}

/**
 * Builds page for submited results view.
 *
 * @param <type> $form
 * @param <type> $form_state
 * @param <type> $entity_id
 * @param <type> $sid
 * @return string
 */
function simpleforms_submit_view_form($form, &$form_state, $entity_id, $sid) {
  $form = _simpleforms_build_drupal_form($form, $form_state, $entity_id, NULL, $sid);

  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $sid
  );

  // Load submission results.
  $submission = simpleforms_db_load_submission($sid);
  
  // We will also check if entity_id that we are getting from url is different from stored submission result nid.
  if (!$submission || $submission->nid != $entity_id) {
    return drupal_not_found();
  }

  // Load author info.
  $author = user_load($submission->uid);
  $form['author'] = array(
    '#type' => 'fieldset',
    '#title' => t('Submission Information'),
    '#weight' => -1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['author']['info'] = array(
    '#markup' => theme('simpleforms_submission_author', array('submission' => $submission, 'account' => $author))
  );

  unset($form['actions']['submit']);
  $form['actions']['ok'] = array(
    '#type' => 'submit',
    '#value' => t('OK'),
    '#submit' => array('simpleforms_submit_view_form_submit_ok'),
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('simpleforms_submit_view_form_submit_delete'),
  );

  return $form;
}

function simpleforms_submit_view_form_submit_ok($form, &$form_state) {
  $form_state['redirect'] = 'node/' . $form_state['values']['entity_id'] . '/simpleforms-results';
}

function simpleforms_submit_view_form_submit_delete($form, &$form_state) {
  $form_state['redirect'] = 'node/' . $form_state['values']['entity_id'] . '/simpleforms-result/' . $form_state['values']['sid'] . '/delete';
}

/**
 * Helper function for form building.
 */
function _simpleforms_build_drupal_form($form, &$form_state, $entity_id, $form_def = NULL, $sid = NULL) {
  // Load form definition.
  if (!$form_def) {
    $simpleform = simpleforms_db_load_form($entity_id);
    $form_def = $simpleform['form_definition'];
    if (empty($form_def)) {
      return;
    }
  }

  $submission_results = NULL;
  if ($sid) {
    // Load submission results.
    $submission_results = simpleforms_db_load_submission($sid);

    // If entity_id is different from stored submission result nid stop execution.
    if (!$submission_results || $entity_id != $submission_results->nid) {
      return;
    }
  }

  // Add form elements.
  require_once 'simpleforms.inc';
  foreach ($form_def as $order => $element) {
    $element['order'] = $order;
    $key = 'simpleforms_' . $element['type'] . '_' . $order;
    $params = array($element);

    // If we have submission results then we need to get submited values for defaults.
    if ($submission_results) {
      if ($element['type'] == 'scale') {
        // We need to find all submit results for this scale.
        $skeys = array_keys($submission_results->data);
        foreach ($skeys as $skey) {
          if (strpos($skey, $key) === 0) {
            $params[1]['default_value'][$skey] = $submission_results->data[$skey]; // default value
            $params[1]['readonly'] = TRUE; // read-only
          }
        }
      }
      else {
        // Usually data[$key] do not exist when form definition is changed after submited data.
        if (isset($submission_results->data[$key])) {
          $params[1]['default_value'] = $submission_results->data[$key]; // default value
          $params[1]['readonly'] = TRUE; // read-only
        }
      }
    }
    
    $form[$key] = call_user_func_array('simpleforms_fapi_' . $element['type'], $params);

    // Is element visible?
    if ($element['visible'] === FALSE) {
      $form[$key]['#access'] = FALSE;
    }
  }

  $form['entity_id'] = array(
    '#type' => 'value',
    '#value' => $entity_id
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**************/
/*** DB API ***/
/**************/

function simpleforms_db_delete_form($entity_id) {
  db_delete('simpleforms_definition')
    ->condition('nid', $entity_id)
    ->execute();
}

/**
 * Returns array with form definition and form options.
 */
function simpleforms_db_load_form($entity_id) {
  $form = _simpleforms_db_load_form($entity_id);
  $form['form_definition'] = drupal_json_decode($form['form_definition']);
  $form['form_options'] = unserialize($form['form_options']);
  return $form;
}

function _simpleforms_db_load_form($entity_id) {
  static $forms = array();

  if (!isset($forms[$entity_id])) {
    $forms[$entity_id] = db_query(
      'SELECT form_definition, form_options FROM {simpleforms_definition} WHERE nid = :nid',
      array(':nid' => $entity_id))
      ->fetchAssoc();
  }

  return $forms[$entity_id];
}

/**
 * Saves simpleform definition to database.
 *
 * @param $entity_id
 * @param $form_data
 * @param $form_options
 */
function simpleforms_db_save_form($entity_id, $form_data, $form_options) {
  db_insert('simpleforms_definition')
    ->fields(array(
      'nid' => $entity_id,
      'form_definition' => $form_data,
      'form_options' => $form_options, ))
    ->execute();
}

/**
 * Loads submission result for the given sid.
 *
 * @param $sid
 *   Submission id.
 * @return
 *   Submission result object on success, or FALSE.
 */
function simpleforms_db_load_submission($sid) {
  static $submissions = array();
  
  if (!isset($submissions[$sid])) {
    $select = db_select('simpleforms_submission', 's');
    $select->join('users', 'u', 's.uid = u.uid');
    $select->fields('s');
    $select->addField('u', 'name', 'username');
    $select->condition('s.sid', $sid);
    $submissions[$sid] = $select->execute()->fetchObject();
    if ($submissions[$sid]) {
      $submissions[$sid]->data = unserialize($submissions[$sid]->data);
    }
  }

  return $submissions[$sid];
}

/**
 * Delete submission result for the given sid.
 *
 * @param $sid
 *   Submission id.
 */
function simpleforms_db_delete_submission($sid) {
  db_delete('simpleforms_submission')->condition('sid', $sid)->execute();
}

/**
 * Delete all submission result for the given entity id.
 *
 * @param $entity_id
 *   Entity id.
 */
function simpleforms_db_delete_all_submissions($entity_id) {
  db_delete('simpleforms_submission')->condition('nid', $entity_id)->execute();
}

/**
 * Saves submission data for given entity_id.
 *
 * @param $entity_id
 * @param $data
 * @return
 *   sid of saved submission data.
 */
function simpleforms_db_save_submission($entity_id, $data) {
  global $user;

  return db_insert('simpleforms_submission')
    ->fields(array(
      'nid' => $entity_id,
      'uid' => $user->uid,
      'time' => REQUEST_TIME,
      'ip' => ip_address(),
      'data' => $data,
    ))
    ->execute();
}
