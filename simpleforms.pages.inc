<?php

/**
 * @file
 * User page callbacks for the simpleforms module.
 */

/**
 * Page for showing submission results for given entity id.
 *
 * @param $entity_id
 * @return <type>
 */
function simpleforms_results_submissions($node) {
  $entity_id = $node->nid;
  $output = '';

  // Create select statement for simpleforms submissions.
  $select = db_select('simpleforms_submission', 's')->extend('PagerDefault');

  // Show submissions just for this entity id.
  $select->condition('s.nid', $entity_id);

  // Join the users table, so we can get the entry creator's username.
  $select->join('users', 'u', 's.uid = u.uid');

  // Select these specific fields for the output.
  $select->addField('s', 'sid');
  $select->addField('s', 'time');
  $select->addField('u', 'name', 'username');

  $rows = $select->execute()->fetchAll(PDO::FETCH_ASSOC);
  if (!empty($rows)) {

    // Number of results selector and all results count.
    $count = $select->getCountQuery()->execute()->fetchField();
    $output .= '<div class="simpleforms-results-per-page">' . t('!20 | !all results per page. !count results total.',
      array(
        '!20' => l('20', 'node/' . $entity_id . '/simpleforms-results', array('query' => array('results' => 20))),
        '!all' => l(t('All'), 'node/' . $entity_id . '/simpleforms-results', array('query' => array('results' => 0))),
        '!count' => $count,
      )) . '</div>';

    $output .= l(t('Export results to csv file'), 'node/' . $entity_id . '/simpleforms-results/export');

    // How many results to show per page.
    if (!isset($_GET['results']) || $_GET['results'] !== '0') {
      $select->limit(20);
    }

    foreach ($rows as &$row) {
      $row['time'] = format_date($row['time']);
      $row['username'] = check_plain($row['username']);
      $row['actions'] = l(t('View'), 'node/' . $entity_id . '/simpleforms-result/' . $row['sid']) . ' | ' .
        l(t('Delete'), 'node/' . $entity_id . '/simpleforms-result/' . $row['sid'] . '/delete');
    }

    // Make a table for them.
    $header = array(t('Id'), t('Time'), t('Submited by'), t('Actions'));
    $output .= theme('table', array('header' => $header, 'rows' => $rows));

    $output .= theme('pager', array('quantity' => 1));
  }
  else {
    drupal_set_message(t('No submission results.'));
  }

  return $output;
}

/**
 * Page for showing submission results for given entity id.
 *
 * @param $entity_id
 * @return
 */
function simpleforms_results_submissions_export($node) {
  $exported_results = simpleforms_results_submissions_export_base($node);
  if ($exported_results==='') {    
    return '';
  }
  else {
    header('Content-Type: "application/octet-stream; charset=utf-8"');
    header('Content-Disposition: attachment; filename="' . $exported_results['file_name'] . '"');
    print $exported_results['file_content'];
    exit();
  }
}

/**
 * Single submission result view page.
 */
function simpleforms_submission_page($node, $sid) {
  global $base_path;

  // Set useable breadcrumb.
  drupal_set_breadcrumb(array(
    l(t('Home'), $base_path),
    l($node->title, 'node/' . $node->nid),
    l(t('Submission results'), 'node/' . $node->nid . '/simpleforms-results'),
  ));

  drupal_set_title(t('Submission result'));

  return drupal_get_form('simpleforms_submit_view_form', $node->nid, $sid);
}

/**
 * Page callback for submission deletion.
 */
function simpleforms_submission_confirm_delete_page($entity_id, $sid) {
  if ($submission = simpleforms_db_load_submission($sid)) {
    return drupal_get_form('simpleforms_submission_confirm_delete', $submission);
  }
  return MENU_NOT_FOUND;
}

/**
 * Form builder; Builds the confirmation form for deleting a single submission.
 */
function simpleforms_submission_confirm_delete($form, &$form_state, $submission) {
  $form['#submission'] = $submission;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $submission->sid,
  );
  return confirm_form(
    $form,
    t('Are you sure you want to delete the submission %sid?', array('%sid' => $submission->sid)),
    'node/' . $submission->nid . '/simpleforms-result/' . $submission->sid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'),
    'submission_confirm_delete'
  );
}

/**
 * Process simpleforms_submission_confirm_delete form submissions.
 */
function simpleforms_submission_confirm_delete_submit($form, &$form_state) {
  $submission = $form['#submission'];
  simpleforms_db_delete_submission($submission->sid);

  drupal_set_message(t('Submission has been deleted.'));
  watchdog('simpleforms', 'Deleted submission @sid.', array('@sid' => $submission->sid));

  $form_state['redirect'] = 'node/' . $submission->nid . '/simpleforms-results';
}

/**
 * Callback for submissions results export to csv file.
 *
 * This function is also used for testing.
 *
 * @param $node
 * @return
 *   array that contain file name and file content
 */
function simpleforms_results_submissions_export_base($node) {
  $entity_id = $node->nid;
  $output = '';

  // Create select statement for simpleforms submissions.
  $select = db_select('simpleforms_submission', 's');

  // Show submissions just for this entity id.
  $select->condition('s.nid', $entity_id);

  // Join the users table, so we can get the entry creator's username.
  $select->join('users', 'u', 's.uid = u.uid');
  $select->join('simpleforms_definition', 'd', 's.nid = d.nid');
  $select->join('node', 'n', 'n.nid = s.nid');

  // Select these specific fields for the output.
  $select->addField('s', 'sid');
  $select->addField('s', 'time');
  $select->addField('s', 'data');
  $select->addField('d', 'form_definition');
  $select->addField('u', 'name', 'username');
  $select->addField('n', 'title', 'node_title');

  $rows = $select->execute()->fetchAll(PDO::FETCH_ASSOC);

  if (!empty($rows)) {
    $first_row = current($rows);
    $file_name = check_plain($first_row['node_title']) . ' - Submission results.csv';

    $header = t('"Id", "Time", "Submited by"');

    // $form_structures contain field titles
    $form_structures = drupal_json_decode($first_row['form_definition']);
    reset($form_structures);
    foreach ($form_structures as $form_structure) {
       if ($form_structure['type'] == 'scale') {
        $fsv = $form_structure['values'];
        reset($fsv);
        while (next($fsv) != FALSE) {
          $scale_item = current($fsv);
          $scale_item_title = current($scale_item);
          $header .= ',"' . check_plain($form_structure['title']) . ' - ' . current($scale_item_title) . '"';
        }
      }
      else {
        $header .= ',"' . check_plain($form_structure['title']) . '"';
      }
    }

    foreach ($rows as &$row) {
      $submited_values = '';

      // $datas contain entered data from survay's fields
      $datas = unserialize($row['data']);
      foreach ($datas as $data) {
        if (is_string($data)) {
          $submited_values .= ',"' . check_plain($data) . '"';
        }
        elseif (is_array($data)) {
          // for check box field there can be multiple values
          foreach ($data as $key => $value) {
            if ($key === $value) {
              $submited_values .= ',"' . check_plain($value) . '"';
            }
          }
        }
      }
      $output .= "\n" . '"' . $row['sid'] . '", "' . format_date($row['time']) . '", "' . check_plain($row['username']) . '"' . $submited_values;
    }

    return array('file_name' => $file_name, 'file_content' => ($header . $output));
  }
  else {
    drupal_set_message(t('No submission results.'));
    return '';
  }

}
