<?php

/**
 * @file
 * Functions for transforming simpleforms form elements definitions to real
 * Drupal form elements.
 */

/**
 * Returns textfield Drupal form element.
 */
function simpleforms_fapi_input_text($element, array $params = array()) {
  $felement = _simpleforms_fapi_text_element($element, $params);
  $felement['#type'] = 'textfield';

  return $felement;
}

/**
 * Returns textarea Drupal form element.
 */
function simpleforms_fapi_textarea($element, array $params = array()) {
  $felement = _simpleforms_fapi_text_element($element, $params);
  $felement['#type'] = 'textarea';

  return $felement;
}

/**
 * Returns select Drupal form element.
 */
function simpleforms_fapi_select($element, array $params = array()) {
  $felement = _simpleforms_fapi_options_element($element, $params);
  $felement['#type'] = 'select';
  $felement['#multiple'] = $element['multiple'] == 'true' ? TRUE : FALSE;

  return $felement;
}

/**
 * Returns select Drupal form element.
 */
function simpleforms_fapi_checkbox($element, array $params = array()) {
  $felement = _simpleforms_fapi_options_element($element, $params);
  $felement['#type'] = 'checkboxes';

  return $felement;
}

/**
 * Returns select Drupal form element.
 */
function simpleforms_fapi_radio($element, array $params = array()) {
  $felement = _simpleforms_fapi_options_element($element, $params, TRUE);
  $felement['#type'] = 'radios';

  return $felement;
}

/**
 * Returns scale options custom form element.
 */
function simpleforms_fapi_scale($element, array $params = array()) {
  $values = & $element['values'];
  $rows = count($values);
  $cols = count($values[0]);
  if ($rows <= 1 || $cols <= 1) {
    return array();
  }

  $felement['scale'] = array(
    '#prefix' => '<div class="form-item form-item-scale">',
    '#suffix' => '</div>',
  );

  // Scale label.
  $felement['scale']['label'] = array(
    '#markup' => '<label>' . check_plain($element['title']) . ' ' . theme('form_required_marker') . '</label>',
  );

  // Table header - scale labels.
  $header = '<table><tr>';
  $header .= '<td></td>';
  for ($i = 1; $i < $cols; ++$i) {
    $header .= '<td>' . check_plain($values[0][$i]['value']) . '</td>';
  }
  $header .= '</tr>';
  $felement['scale']['header'] = array(
    '#markup' => $header,
  );

  // Table scales options.
  $felement['scale']['scales'] = array();
  $scales = & $felement['scale']['scales'];
  for ($i = 1; $i < $rows; ++$i) {
    $scale_key = 'simpleforms_' . check_plain($element['type']) . '_' . $element['order'] . '_' . $i;

    // Radios options.
    for ($j = 1; $j < $cols; ++$j) {
      $options[check_plain($values[0][$j]['value'])] = '';
    }

    // Defaults.
    $default = NULL;
    if (isset($params['default_value']) && !empty($params['default_value'][$scale_key])) {
      $default = check_plain($params['default_value'][$scale_key]);
    }
    else {
      for ($j = 1; $j < $cols; ++$j) {
        if ($values[$i][$j]['checked'] == 'true'? TRUE : FALSE) {
          $default = check_plain($values[0][$j]['value']);
        }
      }
    }

    $scales[$i][$scale_key] = array(
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $default,
      '#scale_option' => '<td><div class="scale-option">' . check_plain($values[$i][0]['value']) . '</div></td>',
      '#theme_wrappers' => array('simpleforms_scale_radios'),
    );
    $scales[$i][$scale_key]['#process'] = array('form_process_radios', 'simpleforms_scale_process_radios');

    if (isset($params['readonly'])) {
      $scales[$i][$scale_key]['#attributes']['readonly'] = 'readonly';
    }

  }

  $felement['scale']['footer'] = array(
    '#markup' => '</table><div class="description">' . check_plain($element['description']) . '</div>',
  );

  return $felement;
}

/************************/
/*** Helper functions ***/
/************************/

/**
 * Process our custom scale radios group.
 */
function simpleforms_scale_process_radios($element) {
  foreach ($element as $key => & $value) {
    if (is_array($value) && isset($value['#type']) && $value['#type'] == 'radio') {
      $value['#theme'] = 'simpleforms_scale_radio';
      $value['#prefix'] = '<td>';
      $value['#suffix'] = '</td>';

      unset($value['#theme_wrappers']);
    }
  }

  return $element;
}

/**
 * Returns generic text input element.
 */
function _simpleforms_fapi_text_element($element, $params) {
  $felement = array(
    '#title' => check_plain($element['title']),
    '#description' => check_plain($element['description']),
    '#required' => $element['required'] == 'true' ? TRUE : FALSE,
  );

  if (isset($params['default_value'])) {
    $felement['#default_value'] = $params['default_value']; // No need for check_plain because theme function will do this.
  }
  if (isset($params['readonly'])) {
    $felement['#attributes']['readonly'] = 'readonly';
  }

  return $felement;
}

/**
 * Returns generic options input element.
 */
function _simpleforms_fapi_options_element($element, $params, $radios = FALSE) {
  $options = array(
    '#title' => check_plain($element['title']),
    '#description' => check_plain($element['description']),
    '#required' => $element['required'] == 'true' ? TRUE : FALSE,
  );

  foreach ($element['values'] as $key => $value) {
    $form_value = check_plain($value['value']);
    $options['#options'][$form_value] = $form_value;

    if (isset($params['default_value'])) { // we have stored default values.

      // Find correct value.
      if (is_array($params['default_value'])) { // multiselect and checkboxes
        $default_values = current($params['default_value']);
        next($params['default_value']);
      }
      else { // radios and single select
        $default_values = $params['default_value'];
      }

      if ($radios) {
        $options['#default_value'] = check_plain($default_values);
      }
      else {
        $options['#default_value'][] = check_plain($default_values);
      }
    }
    elseif ($value['checked'] == 'true') { // take default values from form definition.
      if ($radios) {
        $options['#default_value'] = $form_value;
      }
      else {
        $options['#default_value'][] = $form_value;
      }
    }
  }

  if (isset($params['readonly'])) {
    $options['#attributes']['readonly'] = 'readonly';
  }

  return $options;
}
