<?php

/**
 * @file
 * Theme functions.
 */

function theme_simpleforms_submission_author($variables) {
  $output = '<div class="simpleforms-submission-author">';
  $output .= theme('user_picture', array('account' => $variables['account']));
  $output .= '<div class="username">' . t('Submitted by !username', array('!username' => theme('username', array('account' => $variables['account'])))) . '</div>';
  $output .= '<div class="time">' . format_date($variables['submission']->time, 'long') . '</div>';
  $output .= '<div class="ip">' . $variables['submission']->ip . '</div>';
  $output .= '</div>';
  return $output;
}

function theme_simpleforms_scale_radios($variables) {
  $element = $variables['element'];
  $attributes = array();
  if (isset($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  $attributes['class'] = 'form-radios';
  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] .= ' ' . implode(' ', $element['#attributes']['class']);
  }
  
  return '<tr' . drupal_attributes($attributes) . '>' . $element['#scale_option'] . (!empty($element['#children']) ? $element['#children'] : '') . '</tr>';
}

function theme_simpleforms_scale_radio($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'radio';
  element_set_attributes($element, array('id', 'name', '#return_value' => 'value'));

  if (isset($element['#return_value']) && check_plain($element['#value']) == $element['#return_value']) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-radio'));

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}
