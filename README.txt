
Description
-----------

This module adds simpleform node type to your Drupal site. Typical use 
cases for Simpleforms are surveys, questionnaires and contact/request/ 
register forms.

Submissions from a simpleforms are saved in a database table and can optionally
be mailed to a multiple email address upon submission. Past submissions are
viewable for users with the correct permissions.

Simpleforms includes simple reporting tools and is also capable of exporting
submitted data to a csv file for detailed statistical calculations.

Simpleforms module is similar to webform module but it has lighter
implementation than the webform and it is much simpler to use. The main idea behind
simpleforms module is  keep it simple for the end user. That also means that 
simpleforms will never grown into a very complex and big module
that is difficult to use & administer. Instead, simpleforms will have
pluggable architecture so other contributed modules can easily add advanced
functionality when it is needed.

Features:
- Main feature of simpleforms is very easy custom forms creation process.
  Very complex forms can be created in couple of seconds.
- Supported form elements:
  - Text field,
  - Text area,
  - Select field (single/multiple),
  - Checkbox group,
  - Radio group,
  - Grid (scale) of radio groups.
- Possibility to add custom submit actions to every simpleform form:
  - Custom 'thank you' message,
  - Redirection to custom site page,
  - Notification of new submissions over email.
- Report listing of all submissions for a given simpleform. Also single detail
  view of some submission,
- Export of submitted data to csv file.

Inspiration for this module was jquery.formbuilder plugin
http://www.botsko.net/blog/2009/04/07/jquery-form-builder-plugin/


Requirements
------------

Simpleforms dynamic form creation should work on every modern web browser with
javascript support (including IE6). You will not be able to create forms if
javascript is disabled in your browser.

You will need to download and manually add json2.js, so be sure to check 
the installation section below.


Installation
------------

- Download and unpack module as usual.
- Download json2.js file from https://github.com/douglascrockford/JSON-js and
  put it in simpleforms js folder, for example
  sites/all/modules/simpleforms/js/json2.js.
- Optionally, you can use jquery.scrollTo-min.js for nice scrolling effects while
  adding new fields in simpleforms. Download it from
  http://flesler-plugins.googlecode.com/files/jquery.scrollTo-1.4.2-min.js and
  save it into simpleforms js folder, for example
  sites/all/modules/simpleforms/js/jquery.scrollTo-min.js
- Enable simpleforms module


Configuration
-------------

- Visit Drupal permissions page and configure simpleform content type
  permissions as needed:
  - "Simpleform: Create new content" permissions gives user ability to create
    new forms.
  - "Simpleform: Edit own content" gives form author permission to 
    view/delete/export submitted results.
  - "Simpleform: Delete own content" gives form author permission to delete
    form and all submitted data.


Plans
-----

Stable 1.0
- Better UI for form creation.
- More refactoring of javascript part.
- Advanced statistics (texts and graphs). This will be an additional module.
- Fieldable simpleforms - add simpleforms to any Drupal entity (content types,
  users...). This will maybe go to 2.0 ver.

Stable 2.0
- Try to remove json2.js dependency.
- More fields like file and email for example.
- Pluggable architecture so other modules can add their own custom form fields?
- Rules integration?


Authors
-------

Current maintainers:
- Puljic Ivica 'pivica' <http://drupal.org/user/41070>
