
/**
 * @file
 * Main simpleform JavaScript logic.
 *
 * Inspired by jQuery Form Builder Plugin
 * (http://www.botsko.net/blog/2009/04/jquery-form-builder-plugin/).
 */

(function($) {

/**
 * jQuery SimpleForms Form Builder Plugin.
 */
$.fn.formbuilder = function() {

  var formBuilderGetId = function (i) {
    return 'frm-' + i + '-fld';
  }

  var getInputField = function(type, last_id, value) {
    return '<input class="simpleforms-' + type + '" id="' + type + '-' + last_id + '" type="text" value="' + value + '" />';
  }

  return this.each(function() {
    var ul_obj 		 = this;
    var field      = '';
    var field_type = '';
    var last_id 	 = 1;

    // @@TODO - Internal functions - this should be refactored in some class maybe.

    // Wrapper for adding a new field.
    var appendNewField = function(type, values, options, field_def) {

      if (typeof(field_def) == 'undefined') {
        field_def = {
          visible: true,
          checked: false
        };
      }

      field = '';
      field_type = type;

      // @TODO - refactor
      if(typeof(values) == 'undefined' && type != 'scale') {
        values = [];
        values[0] = Drupal.t('Title');
        values[1] = Drupal.t('Description');
      }

      // @TODO - refactor
      if(typeof(values) == 'undefined' && type == 'scale') {
        values = {};
        values.title = Drupal.t('Title');
        values.description = Drupal.t('Description');
      }

      // @TODO - refactor
      if(typeof(options) == 'undefined') {
        options = [];
        options[0] = Drupal.t('Title');
        options[1] = Drupal.t('Description');
      }

      switch(type) {
        case 'input_text':
          appendTextInput(values, field_def);
          break;
        case 'textarea':
          appendTextarea(values, field_def);
          break;
        case 'checkbox':
          appendCheckboxGroup(values, options, field_def);
          break;
        case 'radio':
          appendRadioGroup(values, options, field_def);
          break;
        case 'select':
          appendSelectList(values, options, field_def);
          break;
        case 'scale':
          appendScaleGroup(values, field_def);
          break;
      }
    }

    // Single line input type="text".
    var appendTextInput = function(values, field_def) {
      field += '<div class="frm-fld"><label>Label:</label>';
      field += getInputField('title', last_id, values[0]) + '</div>';
      field += '<div class="frm-fld"><label>Help:</label>';
      field += getInputField('description', last_id, values[1]) + '</div>';
      help = '';

      appendFieldLi('Text Field', field, field_def, help);
    }

    // Multi-line textarea.
    var appendTextarea = function(values, field_def) {
      field += '<div class="frm-fld"><label>Label:</label>';
      field += getInputField('title', last_id, values[0]) + '</div>';
      field += '<div class="frm-fld"><label>Help:</label>';
      field += getInputField('description', last_id, values[1]) + '</div>';
      help = '';

      appendFieldLi('Paragraph Field', field, field_def, help);
    }

    // Adds a checkbox element group.
    var appendCheckboxGroup = function(values, options, field_def) {

      var title = '';
      var description = '';
      if(typeof(options) == 'object'){
        title = options[0];
        description = options[1];
      }

      field += '<div class="chk_group">';
      field += '<div class="frm-fld"><label>Title:</label>';
      field += getInputField('title', last_id, title) + '</div>';
      field += '<div class="frm-fld"><label>Help:</label>';
      field += getInputField('description', last_id, description) + '</div>';
      field += '<div class="false-label">Select Options</div>';
      field += '<div class="fields">';

      if(typeof(values) == 'object') {
        for(c in values){
          field += checkboxFieldHtml(values[c]);
        }
      } else {
        field += checkboxFieldHtml('');
      }

      field += '<div class="add-area"><a href="#" class="add add_ck">Add</a></div>';
      field += '</div>';
      field += '</div>';

      help = '';
      appendFieldLi('Checkbox Group', field, field_def, help);

      $('.add_ck').live('click', function(){
        $(this).parent().before( checkboxFieldHtml() );
        return false;
      });
    }

    // Returns checkbox field html, since there may be multiple.
    var checkboxFieldHtml = function(values) {
      var checked = false;
      var value = '';

      if(typeof(values) == 'object') {
        value = values[0];
        checked = values[1];
      }

      field = '';
      field += '<div>';
      field += '<input type="checkbox"'+(checked ? ' checked="checked"' : '')+' /><input type="text" value="'+value+'" />';
      field += '<a href="#" class="remove" title="Are you sure you want to remove this checkbox?">Remove</a>';
      field += '</div>';

      return field;

    }

    // Adds a radio element group.
    var appendRadioGroup = function(values, options, field_def) {

      var title = '';
      var description = '';
      if(typeof(options) == 'object'){
        title = options[0];
        description = options[1];
      }

      field += '<div class="rd_group">';
      field += '<div class="frm-fld"><label>Title:</label>';
      field += getInputField('title', last_id, title) + '</div>';
      
      field += '<div class="frm-fld"><label>Help:</label>';
      field += getInputField('description', last_id, description) + '</div>';

      field += '<div class="false-label">Select Options</div>';
      field += '<div class="fields">';

      if(typeof(values) == 'object'){
        for(c in values){
          field += radioFieldHtml(values[c], 'frm-'+last_id+'-fld');
        }
      } else {
        field += radioFieldHtml('', 'frm-'+last_id+'-fld');
      }

      field += '<div class="add-area"><a href="#" class="add add_rd">Add</a></div>';
      field += '</div>';
      field += '</div>';
      help = '';

      appendFieldLi('Radio Group', field, field_def, help);

      $('.add_rd').live('click', function(){
        $(this).parent().before( radioFieldHtml(false, $(this).parents('.frm-holder').attr('id')) );
        return false;
      });
    }

    // Returns radio field html, since there may be multiple.
    var radioFieldHtml = function(values, name) {
      var checked = false;
      var value = '';

      if(typeof(values) == 'object') {
        value = values[0];
        checked = values[1];
      }

      field = '';
      field += '<div>';
      field += '<input type="radio"'+(checked ? ' checked="checked"' : '')+' name="radio_'+name+'" /><input type="text" value="'+value+'" />';
      field += '<a href="#" class="remove" title="Are you sure you want to remove this radio button option?">Remove</a>';
      field += '</div>';

      return field;
    }

    // Adds a scale element group.
    var appendScaleGroup = function(field_def, field_def1) {
      var title = '';
      var description = '';
      var last_scale_id = 0;
      var values = '';

      var scaleNo = 5;
      if (typeof(field_def) == 'object') {
        title = field_def.title;
        description = field_def.description;

        if (typeof(field_def.values) == 'object') {
          values = field_def.values;
          scaleNo = parseInt(values[0][0].value);
        }
      }

      field += '<div class="simpleforms-scale">';

      field += '<div class="frm-fld"><label>Question title:</label>';
      field += getInputField('title', last_id, title) + '</div>';

      field += '<div class="frm-fld"><label>Help:</label>';
      field += getInputField('description', last_id, description) + '</div>';

      field += '<div class="fields">';
      field += '<table>';

      field += '<tr>';
      field += '<td></td>';
      field += '<td>';
      field += 'Number of scales <select class="simpleforms-scale-no">';
      for (s_i = 2; s_i < 11; s_i++) {
        field += '<option value="' + s_i + '"' + (s_i == scaleNo? ' selected="selected"' : '') + '>' + s_i + '</option>';
      }
      field += '</scale>';
      field += 'Labels';
      field += '</td>';
      for (s_i = 1; s_i <= scaleNo; s_i++) {
        field += '<td>';
        field += '<input type="text" value="' + (typeof(values) == 'object' ? values[0][s_i].value : '') + '" class="scale-label"/>';
        field += '</td>';
      }
      field += '</tr>';

      if(typeof(values) == 'object') {
        for (s_i = 1; s_i < values.length; ++s_i) { // Starting from s_i = 1 - skiping table header.
          field += scaleRowHtml(values[s_i], formBuilderGetId(last_id), ++last_scale_id, scaleNo);
        }
      } else {
        field += scaleRowHtml('', formBuilderGetId(last_id), ++last_scale_id, scaleNo);
        field += scaleRowHtml('', formBuilderGetId(last_id), ++last_scale_id, scaleNo);
      }

      field += '<tr><td colspan="2"><div class="add-area"><a href="#" class="add add_sd">Add</a></div></td></tr>';
      field += '</table>';

      field += '</div>';
      help = '';

      appendFieldLi('Scale Group', field, field_def1, help);

      // Register add row click event.
      $('.add_sd').live('click', function() {
        var curScale = $(this).parentsUntil('table').last().find('.simpleforms-scale-no').val();
        $(this).parent().parent().parent().before(scaleRowHtml(false, $(this).parents('.frm-holder').attr('id'), ++last_scale_id, curScale));
        return false;
      });

      // Scale select change event.
      $('select.simpleforms-scale-no').live('change', function(event) {
        // Get all number of scales.
        var newScale = event.currentTarget.value;

        // Get all tr that we need to manipulate.
        var $trs = $(this).parent().parent().parent().find('tr');

        // Get current number of scales.
        var oldScale = $($trs.get(0)).find('td').length - 2;

        var scaleChange = parseInt(newScale - oldScale);

        if (scaleChange < 0) {
          scaleRemoveCol($trs, Math.abs(scaleChange));
        }
        else if (scaleChange > 0) {
          scaleAddCol($trs, Math.abs(scaleChange));
        }

        return false;
      });
    }

    // Adding new td elements to the end of scale.
    var scaleAddCol = function($trs, num) {
      $trs.each(function(index, element) {
        if (index < $trs.length - 1) {
          for (var i = 0; i < num; i++) {
            var lasttd = $(element).find('td').last();
            $(lasttd).clone().insertAfter(lasttd);
          }
        }
      });
    }

    // Removing last td elements from the end of scale.
    var scaleRemoveCol = function($trs, num) {
      $trs.each(function(index, element) {
        if (index < $trs.length - 1) {
          for (var i = 0; i < num; i++) {
            var lasttd = $(element).find('td').last();
            lasttd.remove();
          }
        }
      });
    }

    // Scale row html, since there may be multiple
    var scaleRowHtml = function(values, name, scale_id, scales) {

      var defaults = false;
      if(typeof(values) == 'object') {
        defaults = true;
      }

      var field = '';
      field += '<tr>';
      field += '<td>Option ' + scale_id + '</td>';
      field += '<td><input type="text" id="simpleforms-scale-option-' + scale_id + '"' + 'value="' + (defaults? values[0].value : '') + '" /></td>';
      for (var i = 1; i <= scales; i++) {
        // @TODO - we maybe need real value or whatever...
        field += '<td><input type="radio"' + ((defaults && values[i].checked)? ' checked="checked"' : '') + ' name="simpleforms_scale_radio_' + name + '_' + scale_id + '" /></td>';
      }
      field += '<a href="#" class="scale-remove-row" title="Are you sure you want to remove this scale row?">x</a>';
      field += '</tr>';

      return field;
    }

    // Adds a select/option element.
    var appendSelectList = function(values, options, field_def){

      var multiple = false;
      var title = '';
      var description = '';
      if(typeof(options) == 'object'){
        title = options[0];
        description = options[1];
        multiple = options[2];
      }

      field += '<div class="opt_group">';
      field += '<div class="frm-fld"><label>Title:</label>';
      field += getInputField('title', last_id, title) + '</div>';
      field += '<div class="frm-fld"><label>Help:</label>';
      field += getInputField('description', last_id, description) + '</div>';
      field += '';
      field += '<div class="false-label">Select Options</div>';
      field += '<div class="fields">';
      field += '<input type="checkbox" class="simpleforms-multiple-checkbox" ' + (multiple ? 'checked="checked"' : '') + '><label class="auto">Allow Multiple Selections</label>';

      if(typeof(values) == 'object'){
        for(c in values){
          field += selectFieldHtml(values[c], multiple);
        }
      } else {
        field += selectFieldHtml('', multiple);
      }

      field += '<div class="add-area"><a href="#" class="add add_opt">Add</a></div>';
      field += '</div>';
      field += '</div>';
      help = '';

      appendFieldLi('Select List', field, field_def, help);

      $('.add_opt').live('click', function(){
        $(this).parent().before( selectFieldHtml('', multiple) );
        return false;
      });
    }

    // Select field html, since there may be multiple.
    var selectFieldHtml = function(values, multiple) {
      if (multiple) {
        return checkboxFieldHtml(values);
      } else {
        return radioFieldHtml(values);
      }
    }

    // Appends the new field markup to the editor.
    var appendFieldLi = function(title, field, field_def, help) {
      var li = '';
      li += '<li id="frm-' + last_id + '-item" class="' + field_type + '">';

      li += '<div class="legend">';
        li += '<strong id="txt-title-' + last_id + '">' + title + '</strong>';
        li += '<div class="commands">';
          li += '<a id="move_' + last_id + '" class="move-form" href="#">Move</a>';
          li += '<a id="toggle_' + last_id + '" class="toggle-form" href="#">' + ( field_def.visible? 'Hide' : 'Show') + '</a>';
          li += '<a id="del_' + last_id + '" class="delete-form delete-confirm" href="#"  title="Are you sure you want to delete this form section?"><span>Delete</span></a>'
        li += '</div>';
      li += '</div>';

      li += '<div id="frm-' + last_id + '-fld" class="frm-holder" ' + (field_def.visible ? '' : 'style="display:none"') + '>';
      li += '<div class="frm-elements">';
      li += '<div class="frm-fld"><label for="required-' + last_id + '">Required?</label><input class="required" type="checkbox" value="1" name="required-' + last_id + '" id="required-' + last_id + '"' + (field_def.required ? ' checked="checked"' : '') + ' /></div>';
      li += field;
      li += '</div>';
      li += '</div>';
      li += '</li>';

      $(ul_obj).append(li);
      $('#frm-' + last_id + '-item').hide();
      $('#frm-' + last_id + '-item').animate({
        opacity: 'show',
        height: 'show'
      }, 'slow');

      last_id++;
    }

    // Handle field delete links.
    $('.remove').live('click', function() {
      $(this).parent('div').animate({
        opacity: 'hide',
        height: 'hide',
        marginBottom: '0px'
      }, 'fast', function () {
        $(this).remove();
      });
      return false;
    });

    // Handle field display/hide.
    $('.toggle-form').live('click', function() {
      var action;
      var $this = $(this);
      
      if($this.html() == 'Hide') {
        $this.removeClass('open').addClass('closed').html('Show');
        action = 'hide';
      }
      else {
        $this.removeClass('closed').addClass('open').html('Hide');
        action = 'show';
      }

      $this.closest('li').find('.frm-holder').animate({
        opacity: action,
        height: action
      }, 'slow');

      return false;
    });

    // Handle delete confirmation.
    $('.delete-confirm').live('click', function() {
      var delete_id = $(this).attr('id').replace(/del_/, '');
      if(confirm( $(this).attr('title') )){
        $('#frm-'+delete_id+'-item').animate({
          opacity: 'hide',
          height: 'hide',
          marginBottom: '0px'
        }, 'slow', function () {
          $(this).remove();
        });
      }
      return false;
    });

    // End Internal functions.


    $(ul_obj).html('');

    // Load existing form fields.
    var form_definition = $.parseJSON($('[name=simpleforms_form_definition]').val());
    if(form_definition) {
      var values 		= '';
      var options 	= false;

      for (var i = 0; i < form_definition.length; i++) {
        var field_def = form_definition[i];
        switch (field_def.type) {
          case 'radio':
          case 'checkbox':
          case 'select':
            options    = new Array;
            options[0] = field_def.title;
            options[1] = field_def.description;
            if (field_def.type == 'select') {
              options[2] = field_def.multiple;
            }

            values = new Array;
            for (var j = 0; j < field_def.values.length; ++j) {
              values[j] 	 = new Array(2);
              values[j][0] = field_def.values[j].value;
              values[j][1] = field_def.values[j].checked;
            }

            break;

          case 'scale':
            // @@TODO - here we began refactoring of this stupid transformation which
            // is not needed any more. We already have nicely structured data
            // so no need to transform it one more time.
            // This is done for scale, do it also for other elements.
            options    = new Array;
            values = field_def;
//            options[0] = field_def.title;
//            options[1] = field_def.description;
//
//            values = new Array;
//            for (j = 0; j < field_def.values.length; ++j) {
//              var row = field_def.values[j];
//              values[j] = new Array;
//              for (var k = 0; k < row.length; ++k) {
//                values[j][k] = new Array;
//                values[j][k][0] = row[j].value;
//                if (row[j].checked != null) {
//                  values[j][k][1] = row[j].checked;
//                }
//              }
//            }

            break;

          default:
            values    = new Array;
            values[0] = field_def.title;
            values[1] = field_def.description;
            break;
        }

        appendNewField(field_def.type, values, options, field_def);
      }
    }

    // Create form control select box and add into the editor.
    var select = '';
    select += '<option value="0">Add New Field...</option>';
    select += '<option value="input_text">Text</option>';
    select += '<option value="textarea">Paragraph</option>';
    select += '<option value="checkbox">Checkboxes</option>';
    select += '<option value="radio">Radio</option>';
    select += '<option value="select">Select List</option>';
    select += '<option value="scale">Scale</option>';
    $(this).before('<select name="field_control_top" class="field_control">' + select + '</select>');
    $(this).after('<select name="field_control_bot" class="field_control">' + select + '</select>');
    $('.field_control').change(function() {
      appendNewField($(this).val());
      $(this).val(0).blur();
      if ($.scrollTo) {
        $.scrollTo($('#frm-' + (last_id - 1) + '-item'), 800);
      }
      return false;
    });

    // Register remove row click event.
    $('.scale-remove-row').live('click', function() {
      $(this).parents('tr').remove();
      return false;
    });

  });
};

/**
 * jQuery SimpleForms Form Serialization Plugin.
 */
$.fn.serializeFormList = function(options) {
  // Extend the configuration options with user-provided.
  var defaults = {
    attributes: ['class']
  };
  var opts = $.extend(defaults, options);
  var serialArr = new Array();

  // Begin the core plugin.
  this.each(function() {

    $(this).children().each(function() {

      for(att in opts.attributes) {
        var serialElem = {};

        serialElem.type = $(this).attr(opts.attributes[att]);

        // Append the form field values.
        if(opts.attributes[att] == 'class') {
          var $formElement = $('#' + $(this).attr('id'));

          serialElem.title = $formElement.find('.simpleforms-title').val();
          serialElem.description = $formElement.find('.simpleforms-description').val();
          serialElem.required = $formElement.find('input.required').attr('checked');
          serialElem.visible = $formElement.find('.frm-holder').is(':visible');

          var type = $(this).attr(opts.attributes[att]);
          switch(type) {
            case 'input_text':
            case 'textarea':
              break;
            case 'checkbox':
            case 'radio':
            case 'select':
              serialElem.values = Array();
              if (type == 'select') {
                serialElem.multiple = $formElement.find('.simpleforms-multiple-checkbox').attr('checked');
              }

              $formElement.find('input[type=text]').each(function() {
                if($(this).attr('class') != 'simpleforms-title' && $(this).attr('class') != 'simpleforms-description') {
                  serialElem.values.push({
                    value: $(this).val(),
                    checked: $(this).prev().attr('checked')
                  });
                }
              });
              break;
            
            case 'scale':
              // Scale values, we will visit each row and in it each column.
              serialElem.values = Array();
              $formElement.find('tr').each(function(rowIndex) {
                var $row = $(this).find('input,select');

                // Let's skip rows without things we don't want.
                if ($row.length > 0) {
                  serialElem.values[rowIndex] = [];
                  $row.each(function(colIndex) {
                    serialElem.values[rowIndex][colIndex] = {};
                    var $input = $(this);
                    serialElem.values[rowIndex][colIndex].value = $input.val();
                    if ($input.attr('type') == 'radio') {
                      serialElem.values[rowIndex][colIndex].checked = $input.attr('checked');
                    }
                  });
                }
              });  
              break;
          }
        }

        serialArr.push(serialElem);
      }
    });
  });

  return JSON.stringify(serialArr);
};

})(jQuery);
