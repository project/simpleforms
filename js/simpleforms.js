
/**
 * @file
 * Provides JavaScript additions for simpleform form handling.
 */

(function ($) {

// @TODO - maybe we need to use Drupal.behaviours here.

$('document').ready(function () {
  $('#form-builder').formbuilder();
  
  // Add sorting to form builder.
  $("#form-wrap ul").sortable({
    axis: 'y',
    cursor: 'move',
    forcePlaceholderSize: true,
    handle: '.move-form',
    opacity: 0.6,
    placeholder: 'ui-state-highlight'
  });

  // On form submission first serialize this simpleform form.
  $('#edit-submit').click(function () {
    var $form = $('#edit-simpleforms');

    // Serialize form definition.
    var serForm = $form.find('#form-builder').serializeFormList();

    // Store serialized form into the form definition input filed - we use this
    // field to store serialized form on drupal form submit.
    $form.find('[name=simpleforms_form_definition]').val(serForm);
  });

});

})(jQuery);
